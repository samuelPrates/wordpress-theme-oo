<?php
global $adminMaster;
?>
        <script type="text/javascript">
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            function retornaUrlTemplate(){ return '<?=get_template_directory_uri()?>'; }
            function retornaUrlBase(){ return '<?=home_url()?>'; }
        </script>
        <?php $adminMaster->printScripts(); ?>
    </body>
</html>