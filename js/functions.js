/**
 * Recusão para verificar se há imagens a ser carregadas
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 * @param {string} (Obrigatorio) Item Identificador do bloco a ser verificado exs: "BODY" ou "#slider" 
 * @param {string} (Opcional) Galery Caso esteja utilizando dentro de uma galeria feita com bxslider quando ele termina de baixar a imagem recalcula o tamanho do slider
 */
function recursiveLoadImages(item, galery){
    var $lazy = $(item).find(".lazy").first();
    if($lazy[0]){
        var $load = $lazy.attr("data-src");
        $lazy.attr("src",$load).removeClass("lazy");
        $lazy.on('load',function(){
            if(galery)galery.redrawSlider();
            recursiveLoadImages(item);
        });
    }
}
$(window).load(function(){
    recursiveLoadImages('BODY');
});