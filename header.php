<?php
global $adminMaster;
?><!DOCTYPE html>
<html>
    <head>
        <title><?php if(is_home()) { echo bloginfo('name'); } else { wp_title(''); } ?></title>
        <meta charset="UTF-8">
        <link href="<?=get_template_directory_uri()?>/img/favicon.ico" rel="shortcut icon">
        <?php //getMetas(); ?>
        <?php $adminMaster->printStyles(); ?>
    </head>
    <body>
        <!--Menu-->
        <nav class="navbar">
            <div class="container container-fluid">
                <div class="container row">
                    <div class="col-lg-3">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img src="<?=get_template_directory_uri()?>/img/logo.png" alt="logo"></a>
                    </div>
                    <div id="navbar" class="col-lg-9">
                        <ul class="nav navbar-nav navbar-left">
                            <li class="active"><a href="#">Notícias</a></li>
                            <li><a href="#">Opinião</a></li>
                            <li><a href="#">Perfil</a></li>
                            <li><a href="#">Fotos</a></li>
                            <li><a href="#">Vídeos</a></li>
                            <li><a href="#">Clipping</a></li>
                            <li><a href="#">Redes Sociais</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!--Fim Menu-->
        <!--Slideshow-->
        <div class="container-fluid" id="home-slide">
            <div class="row">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!--INICIO-Carousel Bootstrap -->
                    <?php
                    $banner = new bannerCP();
                    $banner->showHome();
                    ?>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <!--Fim Slideshow Home-->