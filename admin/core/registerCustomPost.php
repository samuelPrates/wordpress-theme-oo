<?php

/**
 * Registra cada novo tipo de postagem
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */
class custom_posts{
    public function __construct($nome, $args) {
        $this->nome     = $nome;
        $this->args     = $args;
        add_action( 'init', array( $this, 'my_custom_post') );
    }
    public function my_custom_post(){
        register_post_type( $this->nome, $this->args );
    }
}