<?php

add_action( 'add_meta_boxes', 'executaBaseBox' );
function executaBaseBox(){
	global $adminMaster;
	foreach($adminMaster->boxes as $box){
		$obj = $box['obj'];
		add_meta_box( $obj->showSlug().'_'.$box['custom'], $obj->showTitulo(), array($obj, 'displayBox'), $box['custom'], 'normal','', array($obj) );
	}
}

/**
 * Modelo extensivo basico para novos boxes
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */
abstract class moduleBaseBox extends moduleBase{

	public function displayBox($post){ return $this; }

	public function saveBox($post_id){ return $this; }

	protected function echoWpField(){
		wp_nonce_field($this->slug, $this->slug.'_wpnonce', false, true );
	}

	protected function verWpField(){
		$this->getParams();
		$theFields = isset ( $this->params[ $this->slug.'_wpnonce' ] )  ? $this->params[ $this->slug.'_wpnonce' ] : "" ;
		return (wp_verify_nonce( $theFields, $this->slug ) );
	}

}