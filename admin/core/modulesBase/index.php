<?php

/**
 * Modelo extensivo basico até agora para os módulos, moduleBaseBox e moduleBaseAdm
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */
abstract class moduleBase{

    protected	$slug		= '';
    protected	$titulo		= '';
	protected	$absPath	= '';
	protected	$baseUrl	= '';

	/**
	* Iniciamento adicional caso necessário
	*/
	public function init( array $args = NULL ){
		return $this;
	}

	public function __construct($slug = NULL, $titulo = NULL, array $args = NULL){
        $this->slug = isset($slug)?$slug:$this->slug;
		$this->titulo = isset($titulo)?$titulo:$this->titulo;
		$this->init(isset($args)?$args:NULL);
    }

	/**
	 * Retorna o slug
	 * @return string Retorna o conteúdo de slug
	 */
	public function showSlug(){
		return $this->slug;
	}

	/**
	 * Retorna o titulo
	 * @return string Retorna o conteúdo de titulo
	 */
	public function showTitulo(){
		return $this->titulo;
	}

	/**
	 * Pega todos os parametros enviados via POST
	 * @return bool Retorna se foi retornado algo via POST
	 */
    public function getParams(){
        $this->params = filter_input_array(INPUT_POST);
        return $this->params <> '';
    }

	/**
	 * Verifica se o $this->slug e $this->titulo estão configurados
	 * @return bool Retorna True caso sejam válidos
	 */
	protected function verSets(){
		return (isset($this->slug, $this->titulo) && $this->slug<>'' && $this->titulo <> '');
	}

	/**
	 * Determina o caminho do módulo.
	 * Não deve ser utilizado pois o sistema já ativa ele automáticamente.
	 */
	public function setAbsPath($path){
		$this->absPath = str_replace('\\', '/', dirname($path));
		$this->baseUrl = (stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].'/'.str_replace($_SERVER['DOCUMENT_ROOT'], '', $this->absPath);
	}
}