<?php


/**
 * Modelo extensivo basico para novas chamadas AJAX
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */
abstract class moduleBaseAjax extends moduleBase{

	public $back	= true;
	public $front	= true;
	public $output	= 'json';
	private $types	= array(
			'json'	=> 'Content-type: application/json',
			'xml'	=> 'Content-Type: text/xml',
			'html'	=> 'Content-Type: text/html'
		);

	private function outputSet(){
		switch($this->output){
			case 'json':
				header($this->types['json']);
			break;
			case 'xml':
				header($this->types['json']);
			break;
			case 'html':
				header($this->types['json']);
			break;
		}
	}

	public function action(){
		$this->outputSet();
		$this->actionAjax();
		exit();
	}

	/**
	 * Ação a ser desenvolvida para resposta no ajax
	 */
	public function actionAjax(){
		exit();
	}

	/**
	 * Ativa o AJAX no sistema.
	 * Não deve ser utilizado pois o sistema já ativa ele automáticamente.
	 */
	public function habilitaAjax(&$obj){
		if($obj->verSets() && is_object($obj) && is_subclass_of($obj,'moduleBaseAjax')){
			if($obj->back){
				add_action('wp_ajax_'.$obj->showSlug(), array($obj,'action'));
			}
			if($obj->front){
				add_action('wp_ajax_nopriv_'.$obj->showSlug(), array($obj,'action'));
			}
		}
	}
}