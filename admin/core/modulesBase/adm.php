<?php

/**
 * Modelo extensivo para modulos administrativos
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */
abstract class moduleBaseAdm extends moduleBase{

    protected	$params = array();
	protected	$config = '';

	/**
	* Iniciamento adicional caso necessário
	*/
	public function init( array $args = NULL ){ 
		$this->getConfig();
		return $this;
	}

	/**
	 * Compara um arquivo de configuração com o do Slug
	 * @param array $config Array a ser comparado
	 */
	protected function comparaConfig($config){
		return (serialize($config) !== serialize($this->config));
	}

	/**
	 * Retorna array com configurações
	 * @return array $config Array a com configurações
	 */
	public function returnConfig(){
		return $this->config;
	}

	/**
	 * Pega as informações iniciais de configuração do slug
	 * @return array Retorna um array de configurações
	 */
	public function getConfig(){
		if($this->verSets()){
			global $adminMaster;
			$this->config = $adminMaster->getParamsAdm()->getBySlug($this->slug);
		}
		return $this->config;
	}

	/**
	 * Salva as informações no banco de dados substituindo as informações antigas para o slug.
	 * @param array $config Array contendo todas as informações de cofiguração do slug
	 * @return object Retorna a si mesmo
	 */
	public function setConfig($config){
		if($this->verSets() && isset($config) && $config<>'' && $this->comparaConfig( $config )){
			global $adminMaster;
			$adminMaster->getParamsAdm()->setBySlug($this->slug, $config);
			$this->getConfig();
		}
		return $this;
	}

	/**
	 * Ação necessária para se utilizar o administrador
	 */
	public function action(){
		return $this;
	}

	/**
	 * Ativa o adm no sistema.
	 * Não deve ser utilizado pois o sistema já ativa ele automáticamente.
	 */
    public function habilitaAdm($obj){
		if($this->verSets() && is_object($obj) && is_subclass_of($obj,'moduleBaseAdm')){
			global $adminMaster;
			$adminMaster->setCustomAdm($this->titulo, $this->slug, array($obj, 'action'));
		}
    }

}