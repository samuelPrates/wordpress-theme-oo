<?php

/**
 * Modelo extensivo basico para novos Custom Posts
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */
abstract class moduleBaseCustomPost extends moduleBase{

	protected $args		= array();
	protected $position	= 0;
	protected $suports	= array();
	protected $boxes	= array();
	protected $categoria = false;

	protected function verSets($c = NULL) {
		return (isset($this->slug, $this->titulo, $this->position, $this->args) && $this->slug<>'' && $this->titulo <> '' && $this->position > 0 && (!empty($this->args) || (isset($c) && $c === FALSE)));
	}

	public function init( array $args = NULL ) {
		if(isset($args)){
			$this->setArgs($args, FALSE);
		} else {
			$this->setArgs($this->args, FALSE);
		}
	}

	/**
	 * Pega os valores dos boxes salvas com o próprio Slug do box
	 * @param integer $id Identificador do item do Custom Post
	 * @return array Array contendo os valores dos boxes cadastrados de acordo com o slug do box
	 */
	public function getBoxValues($id){
		$itens = array();
		foreach ($this->boxes as $box){
			$boxObj = new $box;
			$itens[$boxObj->showSlug()] = get_post_meta( $id, $boxObj->showSlug(), true );
		}
		return $itens;
	}

	protected function setArgs(array $args = NULL, $c = true){
		if($this->verSets($c)){
			$tx = ($this->categoria)?array( 'category', 'post_tag' ):array( 'post_tag' );
			$labels = array(
				'name'               => _x( $this->titulo, ((isset($args['description']) && $args['description'] <> '' && is_string( $args['description'] ))?$args['description']:'Cadastro.') ),
				'singular_name'      => _x( $this->titulo, $this->slug ),
				'add_new'            => _x( ((isset($args['add_new']) && $args['add_new'] <> '' && is_string( $args['add_new'] ))?$args['add_new']:'Adicionar novo'), 'book' ),
				'add_new_item'       => __( ((isset($args['add_new_item']) && $args['add_new_item'] <> '' && is_string( $args['add_new_item'] ))?$args['add_new_item']:'Adicionar novo') ),
				'edit_item'          => __( ((isset($args['edit_item']) && $args['edit_item'] <> '' && is_string( $args['edit_item'] ))?$args['edit_item']:'Editar Item') ),
				'new_item'           => __( ((isset($args['new_item']) && $args['new_item'] <> '' && is_string( $args['new_item'] ))?$args['new_item']:'Novo Item') ),
				'all_items'          => __( ((isset($args['new_item']) && $args['new_item'] <> '' && is_string( $args['new_item'] ))?$args['new_item']:'Lista') ),
				'view_item'          => __( ((isset($args['view_item']) && $args['view_item'] <> '' && is_string( $args['view_item'] ))?$args['view_item']:'Ver') ),
				'search_items'       => __( ((isset($args['search_items']) && $args['search_items'] <> '' && is_string( $args['search_items'] ))?$args['search_items']:'Procurar') ),
				'not_found'          => __( ((isset($args['not_found']) && $args['not_found'] <> '' && is_string( $args['not_found'] ))?$args['not_found']:'Nenhum item encontrado') ),
				'not_found_in_trash' => __( ((isset($args['not_found_in_trash']) && $args['not_found_in_trash'] <> '' && is_string( $args['not_found_in_trash'] ))?$args['not_found_in_trash']:'nenhum item encontrado na Lixeira') ), 
				'parent_item_colon'  => '', 'menu_name' => $this->titulo
			);
			$this->args = array( 'taxonomies' => $tx,'labels' => $labels,'description' => ((isset($args['description']) && $args['description'] <> '' && is_string( $args['description'] ))?$args['description']:'Cadastro.'), 'public' => true, 'menu_position' => $this->position, 'supports' => $this->suports, 'has_archive' => true );
		}
	}

	/**
	 * Ativa o Custom Post no sistema.
	 * Não deve ser utilizado pois o sistema já ativa ele automáticamente.
	 */
    public function habilitaCustomPost(){
		if($this->verSets()){
			global $adminMaster;
			foreach ($this->boxes as $box){
				$iClass	= new ReflectionClass($box);
				$obj = new $box;
				$obj->setAbsPath($iClass->getFileName());
				$adminMaster->boxes[] = array('obj'=>$obj,'custom'=>$this->slug);
				add_action('save_post', array( $obj, 'saveBox' ));
			}
			$adminMaster->setCustomPost($this->slug,$this->args);
		}
    }

}