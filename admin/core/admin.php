<?php

require_once get_template_directory().'/admin/core/admDb.php';
require_once get_template_directory().'/admin/core/registerCustomPost.php';
require_once get_template_directory().'/admin/core/modulesBase/index.php';
require_once get_template_directory().'/admin/core/modulesBase/box.php';
require_once get_template_directory().'/admin/core/modulesBase/adm.php';
require_once get_template_directory().'/admin/core/modulesBase/customPost.php';
require_once get_template_directory().'/admin/core/modulesBase/ajax.php';


/**
 * Administrador Geral
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */
class administrador{

    protected   $styles     = array();
    protected   $scripts    = array();
    public      $baseUrl  = '';
    protected   $pasta    = '';
    protected   $pastas   = array();
    protected   $erros    = array();
    protected   $custAdm  = '';
	public		$boxes	  = array();

	public function autoLoads(){
		foreach(get_declared_classes() as $class){
			switch(true){
				case is_subclass_of($class,'moduleBaseAdm'):
					$this->loadAdms($class);
				break;
				case is_subclass_of($class,'moduleBaseCustomPost'):
					$this->loadCustomPosts($class);
				break;
				case is_subclass_of($class,'moduleBaseAjax'):
					$this->loadCustomAjax($class);
				break;
			}
		}
	}
	private function loadAdms($class){
		$iClass	= new ReflectionClass($class);
		$item	= new $class;
		$item->setAbsPath($iClass->getFileName());
		$item->habilitaAdm($item);
        return $this;
	}

	private function loadCustomPosts($class){
		$item	= new $class;
		$item->habilitaCustomPost($item);
        return $this;
	}

	private function loadCustomAjax($class){
		$iClass	= new ReflectionClass($class);
		$item	= new $class;
		$item->setAbsPath($iClass->getFileName());
		$item->habilitaAjax($item);
        return $this;
	}

    public function inserePadrao(){
        $this->inserePasta('lib')//--------- Inserindo os arquivos de configuração ---------//
        ->inserePasta('modulos');//--------- Inserindo os modulos ---------//
    }
    public function setStyle($url){
        $this->styles[] = sprintf('<link href="%s" media="all" rel="stylesheet" type="text/css">', $url);
        return $this;
    }
    public function setScript($url){
        $this->scripts[] = sprintf('<script src="%s"></script>', $url);
        return $this;
    }
    public function printStyles(){
        foreach ($this->styles as $style){ echo $style."\n"; }
    }
    public function printScripts(){
        foreach ($this->scripts as $script){ echo $script."\n"; }
    }
    protected function setErro($erro){
        $this->erros[] = $erro;
    }
    protected function setPasta($pasta){
        if(is_string($pasta)){
            $this->pasta = $this->baseUrl.$pasta;
            return TRUE;
        } else {
            $this->setErro('O nome da pasta não está correto.');
            return FALSE;
        }
    }

    public function inserePasta($pasta, $c = true){
        if(!$this->setPasta($pasta)){
            return false;
        }
		$p = $this->pasta;
        if ($h = opendir($p)){
            while (false !== ($e = readdir($h))) {
                if ($e != "." && $e != ".." && $e != "index.php" && !is_dir($p.'/'.$e)) {
					//echo $this->pasta.'/'.$e.'<br>';
                    require_once $p.'/'.$e;
                } else if($c && $e != "." && $e != ".." && is_dir($p.'/'.$e)){
                    $this->inserePasta(basename($p).'/'.$e, false);
                }
            }
            closedir($h);
            return $this;
        } else{
            $this->setErro('Esta pasta não foi encontrada.');
            return false;
        }
    }

    public function inserePastas(array $pastas = NULL){
        foreach ($pastas as $pasta){
            if(!$this->inserePasta($pasta)){ break; }
        }
        return $this;
    }

    public function myAdmsBasicos(){

		wp_enqueue_media();
        wp_register_script('adm_generico', get_template_directory_uri() . '/admin/js/adm.js', array('jquery-ui-accordion'));
        wp_register_script('maskedInput', get_template_directory_uri() . '/js/jquery.maskedinput.min.js', array('adm_generico'), '1.0', true);
        wp_register_script('maskedMoney', get_template_directory_uri() . '/js/jquery.maskMoney.min.js', array('adm_generico'), '1.0', true);

        wp_register_style('jquery_ui_adm', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');
    }
    public function __construct($param = NULL) {
        $this->baseUrl = get_template_directory().'/admin/';
        switch(true){
            case isset($param) && is_array($param):
                $this->inserePastas($param);
            break;
            case isset($param) && is_string($param):
                $this->inserePasta($param);
            break;
            default:
                $this->inserePadrao();
            break;
        }
        $this->custAdm = new custom_adm();
        add_action('admin_menu', array($this->custAdm,'registerCustomMenuPage'));
        return $this;
    }

    public function getParamsAdm(){
        return $this->custAdm;
    }

    public function setCustomAdm($name, $slug, $action){
        $this->custAdm->setCustomMenuPage($name, $slug, $action);
    }

    public function setCustomPost($nome, $args){
        new custom_posts($nome, $args);
        return $this;
    }

}