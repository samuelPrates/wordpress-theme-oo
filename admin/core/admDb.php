<?php

/**
 * Inicia e controla os dados de administrador
 * @author Samuel Prates <samuelprates@yahoo.com.br>
 */

class custom_adm{

    protected $arrAdm = array();

    public function __construct(){
        add_action("after_switch_theme", array('custom_adm','ativar'));
    }
    //função que ativa o plugin
    public function ativar(){
        global $wpdb;

        // aqui em baixo vc cria a tabela que usará para o aplicativo. segue exemplo:
        $sql = "CREATE TABLE `".$wpdb->prefix."theme_admin` (
        `id` int(1) NOT NULL auto_increment,
        `slug` varchar(100) NOT NULL default '',
        `config` longtext NOT NULL default '',
        PRIMARY KEY  (`id`))";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    public function getBySlug($slug){
        global $wpdb;
        $config = array();
        $result = $wpdb->get_row($wpdb->prepare("SELECT * FROM `".$wpdb->prefix."theme_admin` WHERE slug = %s order by id DESC", $slug));
        if(!empty($result) && isset($result->config) && is_string( $result->config )){
            $config = unserialize($result->config);
        } else {
            $wpdb->get_row($wpdb->prepare("INSERT INTO `".$wpdb->prefix."theme_admin` (slug, config) VALUES (%s,%s) ", $slug, serialize($config)));
        }
        return $config;
    }

    public function setBySlug($slug, array $config){
        global $wpdb;
        $wpdb->update($wpdb->prefix."theme_admin", array('config' => serialize($config)), array('slug'=>$slug),array('%s'),array('%s'));
    }

    public function setCustomMenuPage($name, $slug, $action){
        $this->arrAdm[] = array($name, $slug, $action);
    }

    public function registerCustomMenuPage(){
        foreach ($this->arrAdm as $adm){
            add_menu_page( $adm[0], $adm[0], 'manage_options', $adm[1], $adm[2] );
        }
    }

}
