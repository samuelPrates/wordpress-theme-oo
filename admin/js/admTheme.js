jQuery(function($) {
    function open_media_window(){
        var window = wp.media({
            title: 'Imagem modulo prefeito',
            library: {type: 'image'},
            multiple: false,
            button: {text: 'trocar'}
        });
        window.on('open',function() {
            var selection = window.state().get('selection');
            ids = $('#imagem').val().split(',');
            ids.forEach(function(id) {
                attachment = wp.media.attachment(id);
                attachment.fetch();
                selection.add( attachment ? [ attachment ] : [] );
            });
        });
        window.on('select', function(){
            var files = window.state().get('selection').toArray();
            $('.imagem-uploaded-photo').html('');
            var imgs = '';
            for (i=0;i<files.length;i++){
                var item = files[i].toJSON();
                if(i>0){ imgs +=','; }
                imgs+=item.id;
                $('.imagem-uploaded-photo').append('<img src="'+item.url+'">');
            }
            $('#imagem').val(imgs);
        });
        window.open();
    }
    $(document).ready(function(){
        $('.add_img_button').click(open_media_window);
    });
});