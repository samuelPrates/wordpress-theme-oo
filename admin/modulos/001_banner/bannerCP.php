<?php

class bannerCP extends moduleBaseCustomPost{

    protected $slug     = 'banner';
    protected $titulo   = 'Banner';
    protected $position = 1;
    protected $suports  = array('title','thumbnail');
    protected $boxes    = array('boxDescricao');
    protected $count    = 2;

    protected function selectAnimation($int){
        switch(true){
            case ($int%6) == 0: return ' lightSpeedIn'; break;
            case ($int%5) == 0: return ' bounceInDown'; break;
            case ($int%4) == 0: return ' bounceInUp'; break;
            case ($int%3) == 0: return ' bounceInRight'; break;
            case ($int%2) == 0: return ' bounceInLeft'; break;
        }
    }

    protected function showCaption(){
        $desc           = get_post_meta( get_the_ID(), 'descricao', true );
        $cap            = '';
        if($desc <> ''){
            $cap .= '<div class="carousel-caption">';
            $desc = explode("\n", $desc);
            foreach ($desc as $d){
                $cap .= '<p class="animated' . $this->selectAnimation( $this->count ) . '">' . trim($d) . '</p>';
                $this->count++;
            }
            $cap .= '</div>';
        }
        return $cap;
    }

    public function showHome(){
        $args	= array(
            'posts_per_page'    => -1,
            'post_type'         => $this->slug
        );
        $que        = new WP_Query($args);
        $count      = 0;
        $indicatos  = '';
        $slides     = '';
        while ($que->have_posts()){
            $que->the_post();
            $img	= wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
            if( $img ) {
                $slides		.= '<div class="item';
                $indicatos	.= '<li data-target="#carousel-example-generic" data-slide-to="' . $count . '"';
                if($count == 0){
                    $indicatos  .= ' class="active"';
                    $slides     .= ' active';
                }
                $indicatos	.= '></li>';
                $slides		.= '">';
                $slides		.= '<img data-src="'. $img[0] .'" width="' . $img[1] . '" height="' . $img[2] . '" class="lazy" alt="' . get_the_title() . '">';
                $slides		.= $this->showCaption();
                $slides		.= '</div>';
                $count++;
            }
        }
        echo '
        <!-- Indicators -->
        <ol class="carousel-indicators">' . $indicatos . '</ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">' . $slides . '</div>';
    }

}