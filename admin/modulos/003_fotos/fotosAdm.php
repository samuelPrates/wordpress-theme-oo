<?php
class fotosAdm extends moduleBaseAdm{

    protected   $slug   = 'fotosAdm';//Necessário p/ identificar o modulo de administração
    protected   $titulo = 'Galeria de Fotos';//Necessário p/ nomear link do adm no menu

    public function action(){
    wp_enqueue_script('media_button', $this->baseUrl . '/js/media_gallery.js', array('jquery','jquery-ui-sortable'), '1.0', true);
    wp_enqueue_style('admGalleryCss', $this->baseUrl . '/css/gallery.css',array('jquery_ui_adm'),'1.1','all');

    if($this->getParams()){ $this->setConfig($this->params); }
    ?>
    <form action="" method="post">
        <h2>Galeria de Fotos</h2>
        <br>
        <div class="box right-col metaboxes side">
            <div class="gallery_add_button icon-plus-squared-1 button">Gerenciar Imagens</div>
            <?php
            $gallery_images  = isset($this->config[$this->slug])?$this->config[$this->slug]:'';
            if( is_array($gallery_images) && !empty( $gallery_images ) ) {
                $gallery_images = implode(",", $gallery_images);
            }
            ?>
            <ul class="gallery-uploaded-photos">
                <?php
                if( ! empty($gallery_images) ){
                    $gallery_images_array = explode(",", $gallery_images);
                    for ($i=0; $i < (count($gallery_images_array)); $i++){
                        if($gallery_images_array[$i] <> 0){
                            echo ' <li data-id="'.$gallery_images_array[$i].'"><span class="imagem"><img src="'.wp_get_attachment_url( $gallery_images_array[$i] ).'"></span><span>'
                            .'<span class="titulo"><span>Titulo: </span><span>' . get_post($gallery_images_array[$i])->post_title . '</span></span>'
                            .'<span class="legenda"><span>Legenda: </span><span>' . get_post($gallery_images_array[$i])->post_excerpt . '</span></span>'
                            .'<span class="descricao"><span>Descrição: </span><span>' . get_post($gallery_images_array[$i])->post_content . '</span></span>'
                            .'</span></li> ';
                        }
                    }
                }
                ?>
            </ul>
            <input type="hidden" name="<?=$this->slug?>" value="<?=$gallery_images;?>" id="gallery-images" class="upload_field">
        </div>
        <p><input type="submit" class="button button-primary" value="Salvar alterações"></p>
    </form>
    <?php
    }

    public function showHome(){
        $gallery_images  = isset($this->config[$this->slug])?$this->config[$this->slug]:'';
        if( !is_array($gallery_images) && !empty( $gallery_images ) ) {
            $gallery_images_array = explode(",", $gallery_images);
        }
        $urls = "";
        $count = 0;
        for ($i=0; $i < (count($gallery_images_array)); $i++){
            if($gallery_images_array[$i] <> 0 && $gallery_images_array[$i] <> ''){
                $image = wp_get_attachment_image_src($gallery_images_array[$i],'fotos');
                echo '<img src="'.$image[0].'" height="'.$image[2].'" width="'.$image[1].'" onclick="showLightBox(' . $count . ')">';
                $urls .='<a href="' . pegaUrlImagem( $gallery_images_array[$i] ) . '" data-id="' . $count . '" rel="lightbox[fotos]"></a>';
                $count++;
            }
        }
        echo '</div>'
        . '<div id="galery-fotos">'
        . $urls
        . '</div>';
    }

}