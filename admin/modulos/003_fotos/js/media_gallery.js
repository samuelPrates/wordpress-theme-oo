jQuery(function($) {
    $(document).ready(function(){
        $('.gallery_add_button').click(open_media_window);
    });
    function open_media_window(){
        var window = wp.media({
            title: 'Inserir imagens',
            library: {type: 'image'},
            multiple: true,
            button: {text: 'Inserir'}
        });
        window.on('open',function() {
            var selection = window.state().get('selection');
            ids = $('#gallery-images').val().split(',');
            ids.forEach(function(id) {
                attachment = wp.media.attachment(id);
                attachment.fetch();
                selection.add( attachment ? [ attachment ] : [] );
            });
        });
        window.on('select', function(){
            var files = window.state().get('selection').toArray();
            $('.gallery-uploaded-photos').html('');
            var imgs = '';
            for (i=0;i<files.length;i++){
                var item = files[i].toJSON();
                console.log(item);
                if(i>0){ imgs +=','; }
                imgs+=item.id;
                $('.gallery-uploaded-photos').append('<li data-id="'
                        +item.id
                        +'"><span class="imagem"><img src="'
                        +item.url
                        +'"></span><span>'
                        +'<span class="titulo"><span>Titulo: </span><span>' + item.title +'</span></span>'
                        +'<span class="legenda"><span>Legenda: </span><span>' + item.caption +'</span></span>'
                        +'<span class="descricao"><span>Descrição: </span><span>' + item.description +'</span></span>'
                        +'</span></li>');
            }
            $('#gallery-images').val(imgs);
        });
        window.open();
    }
    $('.gallery-uploaded-photos').sortable({
        update:function(e, ui){
            $ids = $('.gallery-uploaded-photos').sortable( "toArray", {attribute: 'data-id'} );
            $('#gallery-images').val($ids.join(','));
            console.log($('#gallery-images').val());
        }
    });
});