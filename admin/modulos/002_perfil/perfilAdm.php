<?php
class perfilAdm extends moduleBaseAdm{

    protected	$slug	= 'perfilAdm';//Necessário p/ identificar o modulo de administração
    protected	$titulo	= 'Perfil';//Necessário p/ nomear link do adm no menu

    public function action(){
    wp_enqueue_script('admTheme', $this->baseUrl . '/../../js/admTheme.js', array('maskedInput','maskedMoney'), '1.0', true);
    wp_enqueue_style('admThemeCss', $this->baseUrl . '/../../css/admTheme.css',array('jquery_ui_adm'),'1.1','all');

    if($this->getParams()){ $this->setConfig($this->params); }

    $settings = array( 'media_buttons' => false, 'textarea_rows' => 15 );
    ?>
    <form action="" method="post">
        <h2>Texto do Perfil.</h2>
        <div id="accordion">
            <h3>Texto</h3>
            <div> <?php wp_editor((isset($this->config['perfil'])?$this->config['perfil']:''), 'perfil', $settings); ?> </div>
		</div>
        <p><input type="submit" class="button button-primary" value="Salvar alterações"></p>
    </form>
    <?php
    }

    public function showHome(){
        if(isset($this->config['perfil'])){
            echo apply_filters('the_content',$this->config['perfil']);
        }
    }

}