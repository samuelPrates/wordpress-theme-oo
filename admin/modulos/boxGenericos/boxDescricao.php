<?php
class boxDescricao extends moduleBaseBox{
	protected $slug		= 'descricao';
	protected $titulo	= 'Descrição';

	public function displayBox($post) {
		$this->echoWpField();
		wp_enqueue_style('boxGenericoStyle', $this->baseUrl . '/css/style.css',array('jquery_ui_adm'),'1.1','all');
		$url  = get_post_meta( $post->ID, $this->slug, true );
		?>
		<div class="url-class">
			 <dt><textarea name="<?= $this->slug ?>"><?=(isset($url) ? $url : "")?></textarea></dt>
		</div>
		<?php
	}

	public function saveBox($post_id) {
		if(!$this->verWpField()) return FALSE;
		$this->getParams();
		$url  = isset($this->params[ $this->slug]) ? $this->params[ $this->slug] : "";
		if ( current_user_can( 'edit_post', $post_id ) && isset( $url ) ) {
			update_post_meta( $post_id, $this->slug, $url );
		}
	}

}