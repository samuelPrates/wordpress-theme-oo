<?php
class boxUrl extends moduleBaseBox{
	protected $slug		= 'url';
	protected $titulo	= 'Url de direcionamento';

	public function displayBox($post) {
		$this->echoWpField();
		wp_enqueue_style('boxGenericoStyle', $this->baseUrl . '/css/style.css',array('jquery_ui_adm'),'1.1','all');
		$url  = get_post_meta( $post->ID, $this->slug, true );
		?>
		 <div class="url-class">
			 <dt><input type="text" name="<?= $this->slug ?>" value="<?=(isset($url) && $url<>'' ? $url : "")?>"></dt>
		</div>
		<?php
	}

	public function saveBox($post_id) {
		if(!$this->verWpField()) return FALSE;
		$this->getParams();
		$url  = isset($this->params[ $this->slug]) ? $this->params[ $this->slug] : "";
		if ( current_user_can( 'edit_post', $post_id ) && isset( $url ) ) {
			if( strpos( $url, 'http') === FALSE && strpos( $url, 'https') === FALSE && $url <> '' ){ $url = 'http://'.$url; }
			update_post_meta( $post_id, $this->slug, $url );
		}
	}

}