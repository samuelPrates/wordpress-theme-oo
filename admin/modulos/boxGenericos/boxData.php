<?php
class boxData extends moduleBaseBox{
	protected $slug		= 'data';
	protected $titulo	= 'Inserir uma data';

	public function displayBox($post) {
		$this->echoWpField();
		wp_enqueue_script('admTheme', $this->baseUrl . '/js/functions.js', array('jquery-ui-datepicker'), '1.0', true);
		wp_enqueue_style('boxGenericoStyle', $this->baseUrl . '/css/style.css',array('jquery_ui_adm'),'1.1','all');
		$data  = get_post_meta( $post->ID, $this->slug, true );
		if(isset($data) && $data<>''){
			$item = dateMolderBr::fromFormat('Y-m-d', $data);
		}
		?>
		<div class="url-class">
			<dt><input type="text" readonly="readonly" class="data" name="<?= $this->slug ?>" value="<?=(isset($item)?$item:'')?>"></dt>
		</div>
		<?php
	}

	public function saveBox($post_id) {
		if(!$this->verWpField()) return FALSE;
		$this->getParams();
		$url  = isset($this->params[ $this->slug]) ? $this->params[ $this->slug] : "";
		if ( current_user_can( 'edit_post', $post_id ) && isset( $url ) ) {
			$data = dateMolderBr::fromFormat('d/m/Y', $url);
			update_post_meta( $post_id, $this->slug, $data->getFormatDateDB() );
		}
	}

}