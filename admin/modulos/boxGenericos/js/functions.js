jQuery(function($) {
    $.datepicker.regional['br'] = {clearText: 'Desfazer', clearStatus: '',
    closeText: 'Fechar', closeStatus: 'Fechar sem alterar',
    prevText: '<Ante', prevStatus: 'Ver mês anterior',
    nextText: 'Prox>', nextStatus: 'Ver próximo mês',
    currentText: 'Atual', currentStatus: 'Ver o mês atual',
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
    monthStatus: 'Veja mais um mês', yearStatus: 'Ver mais um ano',
    weekHeader: 'Sm', weekStatus: '',
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sabado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
    dayNamesMin: ['Do','Se','Te','Qa','Qi','Se','Sa'],
    dayStatus: 'Use DD como primeiro dia da semana', dateStatus: 'Escolha DD , MM d',
    dateFormat: 'dd/mm/yy', firstDay: 0, 
    initStatus: 'Escolha a data', isRTL: false};
    $.datepicker.setDefaults($.datepicker.regional['br']);
    
    $(document).ready(function(){
        $('.data').each(function(){
            $(this).datepicker();
        });
    });
});