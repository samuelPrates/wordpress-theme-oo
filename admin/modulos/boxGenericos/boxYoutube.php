<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * <iframe width="380" height="214" src="https://www.youtube.com/embed/BfRQvrB_XoA" frameborder="0" allowfullscreen></iframe>
 */
class boxYoutube extends moduleBaseBox{
	protected $slug		= 'youtube';
	protected $titulo	= 'Video do Youtube';

	public function displayBox($post) {
		$this->echoWpField();
		wp_enqueue_style('boxGenericoStyle', $this->baseUrl . '/css/style.css',array('jquery_ui_adm'),'1.1','all');
		$url  = get_post_meta( $post->ID, $this->slug, true );
		?>
		 <div class="url-class">
			 <dt>Link ou identificador do vídeo: (ex: <small>https://www.youtube.com/watch?v=<strong style="font-size:16px;">Dwm_Wt8KrL8</strong></small>)</dt>
			 <dt><input type="text" name="<?= $this->slug ?>" value="<?=(isset($url) && $url<>'' ? $url : "")?>"></dt>
			 <?php if(isset($url) && $url <> ''){ ?>
			 <br>
			 <dt><strong>Vídeo:</strong></dt>
			 <iframe width="380" height="214" src="https://www.youtube.com/embed/<?=$url?>" frameborder="0" allowfullscreen id="youtube"></iframe>
			 <?php } ?>
		</div>
		<?php
	}

	public function saveBox($post_id) {
		if(!$this->verWpField()) return FALSE;
		$this->getParams();
		$url  = isset($this->params[ $this->slug]) ? $this->params[ $this->slug] : "";
		if ( current_user_can( 'edit_post', $post_id ) && isset( $url ) ) {
			if( (strpos( $url, 'v=') !== FALSE || strpos( $url, '.be') !== FALSE) && $url <> '' ){
				$matches = array();
				preg_match('/.*\.be\/([^\&]*).*|.*v=([^\&]*).*/i', $url, $matches);
				switch(true){
				   case $matches[1] <>'':
					   $url = $matches[1];
				   break;
				   case $matches[2] <>'':
					   $url = $matches[2];
				   break;
				}
			}
			update_post_meta( $post_id, $this->slug, $url );
		}
	}

}
