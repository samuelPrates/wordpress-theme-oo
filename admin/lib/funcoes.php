<?php
/**
 * transforma uma string no formato moeda BR(XX 999.999,99)
 * em uma string formatada como float(999999.99) internacional
 * só com 2 casas decimais
*/
function pegaFloat($str){ return number_format(preg_replace(array("/[^0-9\,]/",'/\,/'),array("",'.'),$str),2,'.',''); }
/**
 * transforma uma string formatada como float(999999.99)
 * só com 2 casas decimais no formato moeda BR(XX 999.999,99)
*/
function pegaMoney($str){ return 'R$ '.number_format($str,2,',','.'); }
/**
 * Retorna string no maximo definido
 * @param string $string Conteudo a ser filtrada
 * @param numeric $max Valor máximo da string
 * @param bool $ret Inserir reticencias (padrão false)
 */
function filtraStringTamanho($string, $max, $ret = FALSE){
    if(strlen($string) <= $max){ return $string; }
    $str = substr(str_replace(array("\n\r","\n","\r"), '', $string), 0,$max+1);
    $pos = strrpos($str, ' ');
    return substr($str, 0,($pos !== false?$pos:NULL)).($ret?' [...]':'');
}
/**
 * Retorna src p/ imagem no tamanho definido
 * @param int $id Identificador da imagem
 * @param string $tamanho Nome correspondente ao tamanho desejado da imagem
 */
function pegaUrlImagem($id, $tamanho = NULL){
    $arr = wp_get_attachment_image_src($id , $tamanho);
    return $arr[0];
}