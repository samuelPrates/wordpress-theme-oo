<?php
//        Usage CreateFromFormat
//        if(isset($data)){
//            $objData = dateMolderBr::fromFormat('d/m/Y', $data);
//            echo $objData->getMonthName();
//        }

/**
 * dateMolderBr
 * 
 * The dateMolderBr class extends DateTime providing methods 
 * to get practiced formats in Brazil
 * 
 * @author Samuel Prates <samuelprates@yahoo.com.br> 
 * @license GNU General Public Licence (GPL)
 * @version Release: 1.0
 */
class dateMolderBr extends DateTime{

    /**
     * Retorna o formato de data (sem hora) p/ inserção em Database
     * @return String String de retorno formatada
     */
    public function getFormatDateDB(){
        return $this->format('Y-m-d');
    }

    /**
     * Retorna o formato de data (com hora) p/ inserção em Database
     * @return String String de retorno formatada
     */
    public function getFormatDateTimeDB(){
        return $this->format('Y-m-d H:i:s');
    }

    /**
     * Retorna uma nova instância deste objeto a partir do formato passado.
     * @return Object Instância dateMolderBr objeto
     */
    public static function fromFormat($format, $time){
        return new self(parent::createFromFormat($format, $time)->format('Y-m-d H:i:s'));
    }

    /**
     * Retorna o formato de data (sem hora) Brasileiro
     * @return String String de retorno formatada
     */
    public function formatDate(){
        return $this->format('d/m/Y');
    }

    /**
     * Retorna o formato de data (Com hora) Brasileiro
     * @return String String de retorno formatada
     */
    public function formatDateTime(){
        return $this->format('d/m/Y H:i:s');
    }

    /**
     * Retorna o nome do Mês completo
     * @return String String com nome do mês
     */
    public function getMonthName(){
        switch($this->format('n')){
            case 1:  return 'Janeiro'; break;
            case 2:  return 'Fevereiro'; break;
            case 3:  return 'Março'; break;
            case 4:  return 'Abril'; break;
            case 5:  return 'Maio'; break;
            case 6:  return 'Junho'; break;
            case 7:  return 'Julho'; break;
            case 8:  return 'Agosto'; break;
            case 9:  return 'Setembro'; break;
            case 10: return 'Outubro'; break;
            case 11: return 'Novembro'; break;
            case 12: return 'Dezembro'; break;
        }
    }

    /**
     * Retorna o nome do Mês Abreviado
     * @return String String com nome do mês
     */
    public function getMonthAbr(){
        switch($this->format('n')){
            case 1:  return 'Jan'; break;
            case 2:  return 'Fev'; break;
            case 3:  return 'Mar'; break;
            case 4:  return 'Abr'; break;
            case 5:  return 'Mai'; break;
            case 6:  return 'Jun'; break;
            case 7:  return 'Jul'; break;
            case 8:  return 'Ago'; break;
            case 9:  return 'Set'; break;
            case 10: return 'Out'; break;
            case 11: return 'Nov'; break;
            case 12: return 'Dez'; break;
        }
    }

    /**
     * Retorna o nome do dia da semana completo
     * @return String String com nome do dia da semana
     */
    public function getWeekName(){
        switch($this->format('w')){
            case 0:  return 'Domingo'; break;
            case 1:  return 'Segunda'; break;
            case 2:  return 'Terça'; break;
            case 3:  return 'Quarta'; break;
            case 4:  return 'Quinta'; break;
            case 5:  return 'Sexta'; break;
            case 6:  return 'Sábado'; break;
        }
    }

    /**
     * Retorna o nome do dia da semana Abreviado
     * @return String String com nome do dia da semana
     */
    public function getWeekAbr(){
        switch($this->format('w')){
            case 0:  return 'Dom'; break;
            case 1:  return 'Seg'; break;
            case 2:  return 'Ter'; break;
            case 3:  return 'Qua'; break;
            case 4:  return 'Qui'; break;
            case 5:  return 'Sex'; break;
            case 6:  return 'Sáb'; break;
        }
    }

    public function __toString() {
        return $this->formatDate();
    }

}