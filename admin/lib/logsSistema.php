<?php
 
class geraLog{

    public static $baseUrl = ADMURLLOG;

    protected   $url;
    protected   $nome;
    protected   $data;
    protected   $handler;
    protected   $hOpen = false;
    private     $erro;

    public function __construct($nome, $data = NULL) {
        $this->nome = $nome;
        if(isset($data)){
            $this->data = $data;
            try{
                $this->criaLog();
            } catch (Exception $ex) {
                $this->erro = $ex;
                $this->desconecta();
                exit();
            }
        }
        global $adminMaster;
        define("ADMURLLOG", $adminMaster->baseUrl.'/log/');
    }

    private function montaUrl(){
        if(isset($this->nome) && is_string($this->nome) && $this->nome <> ''){
            $this->url = self::$baseUrl.$this->nome.date('YmdHis').microtime(true).'.txt';
        } else {
            throw new Exception('O nome não foi setado corretamente. Tipo STRING.');
        }
    }

    private function conecta(){
        $this->montaUrl();
        $this->handler  = fopen($this->url, 'w');
        $this->hOpen    = TRUE;
    }

    private function insereString(){
        if(!empty($this->data)){
            return true;
        } else {
            throw new Exception('A informação a ser postada no log não foi declarada.');
        }
    }

    private function insereArray(){
        $result = '';
        foreach($this->data as $val){
            if(!is_string($val)){ throw new Exception('O array deve ser composto apenas por strings.'); }
            $result .= $val."\n\r";
        }
        $this->data = trim($result);
        if(empty($this->data)){
            throw new Exception('A informação a ser postada no log não foi declarada.');
        }
    }

    private function criaLog(){
        $this->conecta();
        if(isset($this->data) && is_string($this->data)){
            $this->insereString();
        } else if(is_array ($this->data)) {
            $this->insereArray();
        } else {
            throw new Exception('Você só pode enviar uma string ou um array unidimensional. ');
        }
        fwrite($this->handler, $this->data);
        $this->desconecta();
    }

    public function alteraNome($nome){
        $this->nome = $nome;
        try {
            $this->montaUrl();
        } catch (Exception $ex){
            $this->erro = $ex;
            $this->desconecta();
            exit();
        }
    }
    public function publicaLog($data){
        $this->data = $data;
        try{
            $this->criaLog();
        } catch (Exception $ex){
            $this->erro = $ex;
            $this->desconecta();
            exit();
        }
    }

    private function desconecta(){
        if($this->hOpen){ 
            fclose($this->handler);
            $this->hOpen = false;
        }
    }

    public function __destruct() {
       if($this->hOpen){
            $this->hOpen = false;
            fclose($this->handler);
        }
    }

}