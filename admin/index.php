<?php
global $adminMaster;
require_once get_template_directory().'/admin/core/admin.php';
$adminMaster = new administrador();
add_action('admin_enqueue_scripts', array(&$adminMaster,'myAdmsBasicos'));
add_action('wp_enqueue_scripts', array(&$adminMaster,'myAdmsBasicos'));

$adminMaster->autoLoads();