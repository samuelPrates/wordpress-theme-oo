<?php
global $adminMaster;
require 'admin/index.php';

/*
 * Configurações padrão
 */
$adminMaster->setStyle('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css')
		->setStyle(get_template_directory_uri() . '/css/bootstrap.min.css')
		->setStyle(get_template_directory_uri() . '/css/bootstrap-theme.min.css')
		->setStyle(get_template_directory_uri() . '/css/jquery.bxslider.css')
		->setStyle(get_template_directory_uri() . '/style.css');

$adminMaster->setScript('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js')
		->setScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js')
	->setScript(get_template_directory_uri().'/js/jquery.maskedinput.min.js')
	->setScript(get_template_directory_uri().'/js/bootstrap.min.js')
	->setScript(get_template_directory_uri().'/js/jquery.bxslider.min.js')
	->setScript(get_template_directory_uri().'/js/functions.js');

add_theme_support( 'post-thumbnails' );

add_image_size( 'homeThumb', 310, 182, TRUE );
add_image_size( 'fotos', 540, 282, false );

/*
 * Fim Configurações padrão
 */


/*
 * Limpeza do menu
 */

function edit_admin_menus() {
    global $menu;
    global $submenu;
     
    remove_menu_page('index.php');
    remove_menu_page('update-core.php');
    remove_menu_page('edit.php');
    remove_menu_page('edit.php?post_type=page');
    remove_menu_page('edit-comments.php');
    remove_menu_page('plugins.php');
    remove_menu_page('tools.php');
    //remove_submenu_page('themes.php','theme-editor.php'); // Remove the Theme Editor submenu
}
add_action( 'admin_menu', 'edit_admin_menus' );

/*
 * Fim Limpeza do menu
 */